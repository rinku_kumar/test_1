const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var invoice = new Schema({
	invoiceId:String,
	uniqueId:String,
	tenant:String,	
	created_on:{type:Date,default:Date.now},
	emailId:String,
	address:String,
	phone:String,
	amount:Number,
	status:String,
	first_name:String,
	last_name:String,
	fatherName:String,
	description:String,
	order_id:String,
	fineApplied:Boolean,
	fineAmount:Number
});
module.exports = mongoose.model('Invoice', Invoice);