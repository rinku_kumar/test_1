const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Order = new Schema({
	trnxid:String,	
	created_on:{type:Date,default:Date.now},
	updated_on :{type:Date,default:Date.now},
	emailId:String,
	phone:String,
	amount:Number,
	status:String,
	first_name:String,
	last_name:String,
	description:String,
	order_id:String
});