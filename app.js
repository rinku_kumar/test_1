/**
 * Module dependencies
 */
var os = require('os');
var cluster = require('cluster');
var fs = require('fs');
var http = require('http');
var https = require('https');
var express = require('express');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var path = require('path');
var crypto = require('crypto');
var mongoose = require('mongoose');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var routes = require('./routes');
var Account = require('./models/account');
var api = require('./routes/api');
//var models = require('./models/excelupload');
var multer = require('multer');
var xlstojson = require("xls-to-json-lc");
var xlsxtojson = require("xlsx-to-json-lc");
var xlsx = require('xlsx');
var json2xls = require('json2xls');
var dbUtil = require('./config/dbUtil');
var ctx = require('./config/ctxconfig');
var cfg = require('./config/config');
var routeIndex = require('./routes/index');
var authUtil = require('./config/authutils');
var CustomStrategy = require('./utils/customstrategy');
var jobs = require('./jobs/payureconcile');
var schedule = require('node-schedule');
var glob = require('glob');
var requestPromise = require('request-promise');
var feeSvc = require('./service/feeSvc.js');

console.log(CustomStrategy);
const token = 'c0c6a0ffc0df0c516d711fba96b713f7';
const getAuthorizationToken = tenantName => `${token} ${tenantName}`;

// const serverEndPoint = 'https://kryptosda.kryptosmobile.com/';
const serverEndPoint = 'http://localhost:8080/';
const apiForSMSIdpUsers = serverEndPoint + 'kryptosds/nuser/smsIdpUsers/get';
// root app
var rootapp = express();
//mount to contextPath
var app = express();
//rootapp.use(ctx.path_context,app);
app.set('ctx', ctx.path_context);
/**
 * Configuration
 */
// all environments
app.use(ctx.path_context, express.static(path.join(__dirname, 'public')));

app.set('port', process.env.PORT || 3030);
app.set('views', __dirname + '/views');
app.set('view engine', 'pug');

app.use(bodyParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(require('cookie-parser')());
app.use(require('body-parser').urlencoded({
    extended: true
}));
app.use(require('express-session')({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false,
    cookie: {
        maxAge: 3600000
    }
}));
app.use(json2xls.middleware);

//require('./models/excelupload')(app);

// passport config
passport.use('local', new LocalStrategy(Account.authenticate()));
passport.use('student', new CustomStrategy());

//launch reconile
//jobs.reconcile();

passport.serializeUser(function(user, done) {
    user.salt = null;
    user.hash = null;
    //console.log('serializeUser : ',user);
    done(null, user);

});
passport.deserializeUser(function(user, done) {
    done(null, user);
});


// as of now kryptos mobile auth this is disable and user data is stored in session, later we will have customized serilizers
//passport.serializeUser(Account.serializeUser());
//passport.deserializeUser(Account.deserializeUser());
// mongoosep

app.use(passport.initialize());
app.use(passport.session());

mongoose.connect(cfg.mongoUrl);



var ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn(ctx.path_context + '/login');



app.all([ctx.path_context + '/student', ctx.path_context + '/student/*'], ensureLoggedIn, function(req, res, next) {
    //console.log('student interceptor');
    if (req.user && req.user.role === 'STUDENT' || req.url.indexOf('/payucallback') != -1) {
        next();
    } else {
        res.status(401).send('UnAuthorized');
    }
});

app.all([ctx.path_context + '/tenant', ctx.path_context + '/tenant/*'], ensureLoggedIn, function(req, res, next) {
    //console.log('api interceptor');
    if (req.user && req.user.role === 'TENANT') {
        next();
    } else {
        res.status(401).send('UnAuthorized');
    }
});

// serve index and view partials
app.get(ctx.path_context, ensureLoggedIn, routes.index);
app.get(ctx.path_context + '/partials/:name', ensureLoggedIn, routes.partials);

app.get(ctx.path_context + "/guest/*", routes.guestHome);
app.get(ctx.path_context + '/feepayment/:name', routes.feepayment);

app.get(ctx.path_context + '/login', routes.login);

var publicroute = require('./routes/publicController.js');
publicroute.controller(app);


app.post(ctx.path_context + '/register', function(req, res) {
    Account.register(new Account({
        username: req.body.username,
        tenant: req.body.tenant,
        role: req.body.role,
        college: req.body.college,
        email: req.body.email,
        enrollment_no: req.body.enrollment_no
    }), req.body.password, function(err, account) {
        if (err) {
            console.log(err);
            return res.render('register', {
                account: account
            });
        }
        var tenant = req.body.username;
        dbUtil.getConnection(function(db) {
            db.collection('T_' + tenant + '_userdetails').createIndex({
                enrollment_no: 1,
                semester: 1
            }, {
                unique: true
            }, function(err, result) {
                if (err) {
                    console.log('failed to create an index');
                }
                res.send(req.body.username + " registered successfully");
            });
        });

        /*passport.authenticate('local')(req, res, function () {
         res.redirect('/');
         });*/
    });
});

app.post(ctx.path_context + '/addProvisioningDetails', (req,res) => {

    var urlOptions = {
        method: 'POST',
        url: serverEndPoint + 'kryptosds/nuser/addProvisioningDetails/create',
        headers: {
                    'Authorization': getAuthorizationToken(req.body.tenantName)
                },
        json: req.body  
    };
    requestPromise(urlOptions)
    .then(response => {
        res.send(response);
    })
    .catch(error => {
        res.status(error.statusCode).send(error.error);
    });
});

app.post(ctx.path_context + '/smsIdpUsers', (req,res) => {
    
    var urlOptions = {
        method: 'POST',
        url: serverEndPoint + 'kryptosds/nuser/smsIdpUsers',
        headers: {
                    'Authorization': getAuthorizationToken(req.body.tenantName)
                },
        json: req.body  
    };
    requestPromise(urlOptions)
    .then(response => {
        res.send(response);
    })
    .catch(error => {
        res.status(error.statusCode).send(error.error);
    });
});

app.post(ctx.path_context + '/sendIdpUsernameOverEmail', (req,res) => {
    
    var urlOptions = {
        method: 'POST',
        url: serverEndPoint + 'kryptosds/nuser/sendIdpUsernameOverEmail',
        headers: {
                    'Authorization': getAuthorizationToken(req.body.tenantName)
                },
        json: req.body  
    };
    requestPromise(urlOptions)
    .then(response => {
        res.send(response);
    })
    .catch(error => {
        res.status(error.statusCode).send(error.error);
    });
});

app.post(ctx.path_context + '/sendInvitationEmails', (req,res) => {

    dbUtil.getConnection(function(db) {
        var collection = db.collection('emails');
        
        collection.find({tenant: req.body.tenant}).toArray(function(err, result) {
            var list = result[0].emailList

            // console.log(emailList[0])
            var urlOptions = {
                method: 'POST',
                url: serverEndPoint + 'kryptosds/nuser/sendInvitationEmails',
                headers: {
                            'Authorization': getAuthorizationToken(req.body.tenantName)
                        },
                json: {list}
            };
            requestPromise(urlOptions)
            .then(response => {
                res.send(response);
            })
            .catch(error => {
                res.status(error.statusCode).send(error.error);
            });

            if (err) {
                console.log(err)
            }
        });
    });
    

});

app.post(ctx.path_context + '/login/tenant', passport.authenticate('local', {
    successRedirect: '/feepayment/tenant#userManagement',
    failureRedirect: '/feepayment/tenant/login'
}));
app.post(ctx.path_context + '/login/student', passport.authenticate('student', {
    successRedirect: '/feepayment/student#student',
    failureRedirect: '/feepayment/login/student'
}));


/*glob("./routes/*Controller.js", function(err, files) {

    files.forEach(function(file) {
        console.log(file)
        var fileRoute = require(file);
        fileRoute.controller(app);
    });
});*/

/*app.post(ctx.path_context + '/login', function(req, res, next) {
    passport.authenticate('local', function(err, user, info) {
        if (err) {
            return next(err);
        }
        if (!user) {
            return res.redirect(ctx.path_context+'/login');
        }
        req.logIn(user, function(err) {
            if (err) {
                return next(err);
            }
            if(user.role=='TENANT')
                return res.redirect('/feepayment/tenant#home');
            if(user.role=='STUDENT'){
                return res.redirect('/feepayment/student#student');
            }
        });
    })(req, res, next);
});*/

app.get(ctx.path_context + '/loginfailure', function(req, res) {
    res.render('login', {
        message: "Username or password is incorrect"
    });
    //res.send('Failed to authenticate');
});
app.get(ctx.path_context + '/logout', function(req, res) {
    req.logout();
    res.redirect(ctx.path_context);
});
var storage = multer.diskStorage({ //multers disk storage settings
    destination: function(req, file, cb) {
        cb(null, './uploads/')
    },
    filename: function(req, file, cb) {
        var datetimestamp = Date.now();
        cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1])
    }
});
var upload = multer({ //multer settings
    storage: storage,
    fileFilter: function(req, file, callback) { //file filter
        if (['xls', 'xlsx'].indexOf(file.originalname.split('.')[file.originalname.split('.').length - 1]) === -1) {
            return callback(new Error('Wrong extension type'));
        }
        callback(null, true);
    }
}).single('file');
//module.exports = function(app) {
//API path that will upload the files 
app.post(ctx.path_context + '/upload', function(req, res) {
    //console.log(req.body);
    var exceltojson;
    upload(req, res, function(err) {
        if (err) {
            res.json({
                error_code: 1,
                err_desc: err
            });
            return;
        }
        //Multer gives us file info in req.file object 
        if (!req.file) {
            res.json({
                error_code: 1,
                err_desc: "No file passed"
            });
            return;
        }
        //Check the extension of the incoming file and 
        //  use the appropriate module
        if (req.file.originalname.split('.')[req.file.originalname.split('.').length - 1] === 'xlsx') {
            exceltojson = xlsxtojson;
        } else {
            exceltojson = xlstojson;
        }
        var workbook = xlsx.readFile(req.file.path);
        var sheets = workbook.SheetNames;
        //console.log(sheets);
        var tenant = req.user.username;
        //var tenant = "ABCD";
        function saveSheetData(index, sheets, tenant, cb) {
            console.log(index + " " + sheets.length);
            try {
                if (!(index < sheets.length)) {
                    console.log('returning');
                    return cb(null, true);
                }
                var sheetName = sheets[index];
                exceltojson({
                    input: req.file.path,
                    output: null,
                    sheet: sheetName,
                    lowerCaseHeaders: true
                }, function(err, result) {

                    if (err) {
                        return cb(err, null)
                    } else {
                        //console.log("results : ",result);
                        dbUtil.getConnection(function(db) {
                            var collection = db.collection('T_' + tenant + '_userdetails');
                            var bulk = collection.initializeUnorderedBulkOp();
                            var currIdx = result.length;
                            //var stuArr=[];
                            for (var j = 0; j < currIdx; j++) {

                                if (result[j].enrollment_no && result[j].enrollment_no.trim() != "" && result[j].semester && result[j].semester.trim() != "") {
                                    result[j].stream = sheetName;
                                    bulk.find({
                                        enrollment_no: result.enrollment_no,
                                        semester: result.semester
                                    }).upsert().updateOne(result[j]);
                                    //bulk.insert(result[j]);

                                }
                                // result[j].stream = sheetName;
                            }
                            bulk.execute(function(err, resData) {
                                console.log('docs saved : ', resData.nInsertted, " docs failed ", resData.writeErrors);
                                index++;
                                saveSheetData(index, sheets, tenant, cb);
                            });

                        });

                    }
                });
            } catch (e) {
                res.redirect(ctx.path_context + '/tenant#home');

            }
        }
        saveSheetData(0, sheets, tenant, function(err, data) {
            if (err) {
                res.redirect(ctx.path_context + '/tenant#home?upload=d');
            }
            /*return res.render('index', {
                           message: 'Unable to save data'
                       });*/
            if (data) {
                console.log('save :: data saved successfully. ');
                res.redirect(ctx.path_context + '/tenant#home');
            }
        })
    })
});
//}




app.get(ctx.path_context + '/tenant/api/download', ensureLoggedIn, function(req, res) {
    var tenant = req.user.username;
    dbUtil.getConnection(function(db) {
        var collection = db.collection('T_' + tenant + '_userdetails');

        collection.find({}, {
            _id: 0,
            trnxid: 0
        }).sort({
            txnid: -1
        }).toArray(function(err, docs) {
            console.log("Docs count", docs.length);
            if (err) {
                console.log(err)
            }
            res.xls(tenant + "_student_fee.xlsx", docs);
        });
    });
});

//api for mobikwik
require('./routes/mobikwikController').controller(app);
// JSON API
var payroute = require('./routes/paymentController.js');
payroute.controller(app);

var feeRoute = require('./routes/feeController.js');
feeRoute.controller(app);

//APIs for User Management
require('./routes/userMgmtController.js')(app);

app.post(ctx.path_context + "/api/update", api.updateTenant);
app.post(ctx.path_context + "/api/validationList", api.validationList);
app.post(ctx.path_context + "/api/uploadEmails", api.uploadEmails);

app.get(ctx.path_context + '/tenant/api/studentdata', ensureLoggedIn, api.studentdata);
//app.get(ctx.path_context + '/tenant/api/studentpagedData', ensureLoggedIn, api.studentpagedData); 

app.get(ctx.path_context + "/details/t", api.fetchTenantDetails);

app.get([ctx.path_context + '/tenant/api/getcurrentUser', ctx.path_context + '/student/api/getcurrentUser'], ensureLoggedIn, api.getCurrentUser);

app.get([ctx.path_context + '/tenant/api/gettenant', ctx.path_context + '/student/api/gettenant'], ensureLoggedIn, api.gettenant);
app.get(ctx.path_context + '/api/student', api.fetchStudent);
app.post(ctx.path_context + '/tenant/api/adddata', ensureLoggedIn, api.adddata);
app.post(ctx.path_context + '/tenant/api/editdata', ensureLoggedIn, api.editdata);
app.post(ctx.path_context + '/tenant/api/deletedata', ensureLoggedIn, api.deletedata);
//app.get(ctx.path_context+'/api/student', ensureLoggedIn, api.fetchStudent);
app.post(ctx.path_context + '/tenant/api/saveentry', ensureLoggedIn, api.saveentry);
//app.get(ctx.path_context + '/tenant/api/metadata/:name', ensureLoggedIn, api.metadata);
//app.get(ctx.path_context + '/tenant/api/kryptos/listMyApps', ensureLoggedIn, api.mykryptosapps);
//app.get(ctx.path_context + '/tenant/api/kryptos/livedata/:tenantid', ensureLoggedIn, api.kryptoslivedata);
app.get(ctx.path_context, ensureLoggedIn, routes.index);
// redirect all others to the index (HTML5 history)
app.get(ctx.path_context + "*", ensureLoggedIn, routes.index);
// global error handler
app.use('*', function(err, req, res, next) {
    console.log("error", err);
    res.json({
        status: 'error',
        message: 'Some thing went wrong'
    });
});
process.on('uncaughtException', function(err) {
    console.log('**************************');
    console.log('* [process.on(uncaughtException)]: err:', err);
    console.log('**************************');
    process.exit(1);
});
//var privateKey = fs.readFileSync('./keys/key.pem').toString();
//var certificate = fs.readFileSync('./keys/cert.pem').toString();
http.createServer( /*{key:privateKey,cert:certificate},*/ app).listen(app.get('port'), function() {
    console.log('Express server listening on port ' + app.get('port'));
    schedule.scheduleJob('0 */10 * * *', jobs.payuReconcile());
});

module.exports = app;