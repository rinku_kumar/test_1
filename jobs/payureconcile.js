var schedule = require('node-schedule');
var dbUtil = require('../config/dbUtil');
var payU = require('../utils/payUUtils');
var payUConfig = require('../config/payU');
var Transaction = require('../models/transaction');
var request = require('request-promise');
var async = require('async');


var ObjectID = require('mongodb').ObjectID;

var payuEndPoint = "https://info.payu.in/merchant/postservice.php";

var reconcileDocCnt = 0;

var userDetailsSvc = require('../service/userDetailsSvc');
var feeSvc = require('../service/feeSvc');



function reconcilePayments(startTime, endDate) {

};
module.exports.payuReconcile = function() {
	console.log("running payment reconcile job..", new Date);
	dbUtil.getConnection(function(db) {
		var joblog = db.collection("schedulejobs");
		joblog.findOne({
			"job_name": "payment_reconcile",
			"status": "completed"
		}, {
			sort: [
				["created_on", -1]
			]
		}, function(err, doc) {
			if (err) {
				console.log("err in log  ", err);
			}
			var startDate;
			var endDate = new Date();
			endDate.setMinutes(endDate.getMinutes() - 10);
			//console.log("data == ",doc);
			if (!doc) {
				startDate = undefined;

			} else {
				startDate = doc.endDate;
			}


			var queryObj = {
				status: "PaymentInitiated"
			};
			if (!startDate) {
				queryObj.created_on = {
					$lte: endDate
				};
			} else {
				queryObj.created_on = {
					$gt: startDate,
					$lte: endDate
				};
			}

			var jobObj = {
				"job_name": "payment_reconcile",
				"endDate": endDate,
				"created_on": new Date(),
				"status": "started"
			};
			if (startDate) {
				jobObj.startDate = startDate;
			}
			var jobId;
			joblog.insertOne(jobObj, function(err, result) {
				jobId = result.ops[0]._id;
				console.log("job..result", jobId);
			});
			payUConfig.payUConfig().then((config) => {
				dbUtil.getConnection(function(db) {
					var trnxColl = db.collection("transactions");

					trnxColl.find(queryObj).toArray(function(err, docs) {
						//console.log("docs..", docs);
						if (!err) {
							async.each(docs, function(doc, callback) {
								var hash = payU.createHashForOtherServices(config, "verify_payment", doc.trnxid);

								var formData = {
									form: "2",
									key: config.key,
									command: "verify_payment",
									hash: hash,
									var1: doc.trnxid
								};

								var reqOptions = {
									method: 'POST',
									uri: payuEndPoint,
									formData: formData,
									headers: {
										'content-type': 'application/x-www-form-urlencoded'
									}
								};
								//console.log("request::", reqOptions);

								request(reqOptions).then((response) => {
									//console.log("checking transaction for", doc.trnxid ,"response received is : ",response.transaction_details);

									if (!response||response.status == "0" || !response.transaction_details||response.transaction_details[doc.trnxid].status == "Not Found") {
										// do nothing
										dbUtil.getConnection(function(db) {
											var trnxColl = db.collection("transactions");
											console.log("updating collection for failed trnx", doc.trnxid);
											trnxColl.update({
												trnxid: doc.trnxid
											}, {
												$set: {
													status: "failure",
												}
											}, function(err, result) {
												callback();

											});
										});

									} else {
										var payuResponse = response.transaction_details[doc.trnxid];
										if (payuResponse.status == "success") {

											if (!doc.fee_for || doc.fee_for != "misc") {
												userDetailsSvc.update(doc.tenant, {
													'enrollment_no': payuResponse.udf1,
													'semester': payuResponse.udf2
												}, {
													status: "Paid",
													txnid: doc.trnxid,
													payment_date: doc.created_on
												}).then((updResult) => {
													dbUtil.getConnection(function(db) {
														console.log("updating transaction for success trx", doc.trnxid);
														var trnxColl = db.collection("transactions");
														trnxColl.update({
															trnxid: doc.trnxid
														}, {
															$set: {
																status: payuResponse.status,
																pg_response: payuResponse
															}
														}, function(err, result) {
															callback();

														});
													});
												}, (err) => {
													callback();
												});
											} else {
												feeSvc.getStudentFee(doc.tenant, {
													fee_id: doc.fee_id
												}).then((stuFee) => {
													console.log("Fee", stuFee);
													stuFee.fee_status = "PAID";
													stuFee.fee.fees.forEach(function(fee) {
														fee.fee_status = "PAID";
													});

													stuFee.txnid = doc.trnxid;
													stuFee.payment_date = doc.created_on;

													feeSvc.updateStudentFee(doc.tenant, {
														fee_id: stuFee.fee_id
													}, stuFee).then((updResult) => {
														dbUtil.getConnection(function(db) {
															console.log("updating transaction for success trx", doc.trnxid);
															var trnxColl = db.collection("transactions");
															trnxColl.update({
																trnxid: doc.trnxid
															}, {
																$set: {
																	status: payuResponse.status,
																	pg_response: payuResponse
																}
															}, function(err, result) {
																callback();

															});
														});
													}, (err) => {
														callback();
													});;


												}, (err) => {
													callback();
												});
											}

											/*dbUtil.getConnection(function(db) {
												var collection = db.collection('T_' + doc.tenant + '_userdetails');
												//console.log("setting user details update for roll",payuResponse.udf1);
												collection.update({
													'enrollment_no': payuResponse.udf1,
													'semester': payuResponse.udf2
												}, {
													$addToSet: {
														"trnxid": doc.trnxid
													},
													$set: {
														status: "Paid",
														txnid: doc.trnxid,
														payment_date: doc.created_on
													}
												}, (err, result) => {
													if (err) {
														//handle error
														console.log(err);
														callback();


													} else {
														dbUtil.getConnection(function(db) {
															console.log("updating transaction for success trx", doc.trnxid);
															var trnxColl = db.collection("transactions");
															trnxColl.update({
																trnxid: doc.trnxid
															}, {
																$set: {
																	status: payuResponse.status,
																	pg_response: payuResponse
																}
															}, function(err, result) {
																callback();

															});
														});
													}
												});
											});*/


										} else if (payuResponse.status == "failure" || payuResponse.status == "pending") {
											dbUtil.getConnection(function(db) {
												var trnxColl = db.collection("transactions");
												trnxColl.update({
													trnxid: doc.trnxid
												}, {
													$set: {
														status: "failure",
														pg_response: payuResponse
													}
												}, function(err, result) {
													callback();
												});
											});
										}
									}
								}).catch(function(err) {
									console.log(err.message);
									console.log("failed to reconcile ..txn ..", doc.trnxid);
								});
							}, function(err, result) {
								console.log("inserting job logg");
								if (!err) {
									joblog.update({
										_id: new ObjectID(jobId)
									}, {
										$set: {
											status: "completed"
										}
									});
								}
							});

						} else {
							console.log("Error occured in reconciling... ", err);

							joblog.update({
								_id: new ObjectID(jobId)
							}, {
								$set: {
									status: "failed"
								}
							});

						}

					});
				});
			});

		});
	});

};